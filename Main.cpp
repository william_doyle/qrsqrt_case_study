#include <iostream>
#include <cmath>

/* // original QUAKE III function
float Q_rsqrt( float number )
{
	long i;
	float x2, y;
	const float threehalfs = 1.5F;

	x2 = number * 0.5F;
	y  = number;
	i  = * ( long * ) &y;						// evil floating point bit level hacking
	i  = 0x5f3759df - ( i >> 1 );               // what the fuck?
	y  = * ( float * ) &i;
	y  = y * ( threehalfs - ( x2 * y * y ) );   // 1st iteration
//	y  = y * ( threehalfs - ( x2 * y * y ) );   // 2nd iteration, this can be removed

#ifndef Q3_VM
#ifdef __linux__
	assert( !isnan(y) ); // bk010122 - FPE?
#endif
#endif
	return y;
}
*/

inline long fast_half(long i ){
	return i >> 1;	
}

float Q_rsqrt( float number ) {
	long i;
	float x2;// = number * 0.5F;
	x2 = number * 0.5F;
	float y;
	const float threehalfs = 1.5F;

	x2 = number * 0.5F;
	y  = number;
	i  = *reinterpret_cast<long*>(&y);							// evil floating point bit level hacking
	i  = 0b01011111001101110101100111011111 - fast_half(i);     // what the fuck?
	y  = *reinterpret_cast<float*>(&i);
	y  *= ( threehalfs - ( x2 * pow(y, 2) ) );   				// 1st iteration

	return y;
}

/*
	---Its best not to think of this as a number but a bit pattern thats why my modifications put it in binary---
 	binary	0b01011111001101110101100111011111
	hex 	0x5f3759df
   	decimal 1597463007 
*/

int main() {
	
	float val = 0.2501;	// the number we want to find the inverse square root of 
	float carmk_rslt = Q_rsqrt(val);
	float rslt = 1.0/sqrt(val);

	std::cout << "Approximation\t" << carmk_rslt << "\n";
	std::cout << "Truth\t\t" << rslt << "\n";
	std::cout << "Difference\t" << rslt - carmk_rslt << "\n";
	
	std::cout << "4 >> 1 is "<< (4 >> 1) << "\n";
	std::cout << "5 >> 1 is "<< (5 >> 1) << "\n";
	std::cout << "6 >> 1 is "<< (6 >> 1) << "\n";
	std::cout << "7 >> 1 is "<< (7 >> 1) << "\n";
	std::cout << "8 >> 1 is "<< (8 >> 1) << "\n";
	std::cout << "8 >> 2 is "<< (8 >> 2) << "\n";
	std::cout << "The magic number is " << 0x5f3759df << "\n"; 
	
	

};
